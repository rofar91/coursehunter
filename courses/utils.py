from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .models import *
from django.core.paginator import Paginator


class ObjectDetailMixin:
    model = None
    template = None

    def get(self, request, id_id):
        categories = Category.objects.all()
        # category =
        subcategory = ''
        if self.model == Course:
            subcategory = SubCategory.objects.get(course=id_id)

        subcategories = ''
        courses = ''
        if self.model == Category:
            subcategories = Category.objects.get(id=id_id).subcategory_set.all()
            courses = Course.objects.filter(subcategory__category_id=id_id)
            # courses = Course.objects.all().prefetch_related('subcategory')  # можно через prefetch оптимизировать

        videos = Video.objects.filter(course_id=id_id)


        paginator = Paginator(courses, 3)
        page_number = request.GET.get('page', 1)
        page = paginator.get_page(page_number)

        is_paginated = page.has_other_pages()

        if page.has_previous():
            prev_url = '?page={}'.format(page.previous_page_number())
        else:
            prev_url = ''

        if page.has_next():
            next_url = '?page={}'.format(page.next_page_number())
        else:
            next_url = ''

        obj = get_object_or_404(self.model, id__iexact=id_id)
        return render(request, self.template, context={
            self.model.__name__.lower(): obj,
            'categories': categories,
            'subcategories': subcategories,
            'subcategory': subcategory,
            'courses': page,
            'videos': videos,
            'is_paginated': is_paginated,
            'next_page_url': next_url,
            'prev_page_url': prev_url,
        })
