# Generated by Django 3.0.5 on 2020-04-15 15:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0017_auto_20200415_1444'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='name',
            new_name='category_name',
        ),
    ]
