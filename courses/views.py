from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.views.generic import View
from .utils import ObjectDetailMixin
from django.core.paginator import Paginator
from django.db.models import Q
from .models import *


# Create your views here.
def courses_list(request):
    search_query = request.GET.get('search', '')

    if search_query:
        courses = Course.objects.filter(Q(title__icontains=search_query))  # | Q(description__icontains=search_query))
    else:
        courses = Course.objects.all()

    categories = Category.objects.all()
    subcategories = SubCategory.objects.all()

    paginator = Paginator(courses, 3)
    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)

    is_paginated = page.has_other_pages()

    if page.has_previous():
        prev_url = '?page={}'.format(page.previous_page_number())
    else:
        prev_url = ''

    if page.has_next():
        next_url = '?page={}'.format(page.next_page_number())
    else:
        next_url = ''

    context = {
        'courses': page,
        'categories': categories,
        'subcategories': subcategories,
        'is_paginated': is_paginated,
        'next_page_url': next_url,
        'prev_page_url': prev_url,
    }

    return render(request, 'courses/index.html', context=context)


def categories_list(request):
    pass


class CourseDetail(ObjectDetailMixin, View):
    model = Course
    template = 'courses/course_detail.html'


class CategoryDetail(ObjectDetailMixin, View):
    model = Category
    template = 'courses/category_detail.html'
