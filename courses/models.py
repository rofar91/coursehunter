from django.db import models
from django.shortcuts import reverse


class Category(models.Model):
    category_name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(max_length=50, unique=True, default='')

    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'id_id': self.id})

    def __str__(self):
        return self.category_name


class SubCategory(models.Model):
    subcategory_name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(max_length=50, unique=True, default='')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    # def get_absolute_url(self):
    #     return reverse('subcategory_detail_url', kwargs={'id_id': self.id})

    def __str__(self):
        return self.subcategory_name


class Course(models.Model):
    title = models.CharField(max_length=100, unique=True)
    image = models.CharField(max_length=200, blank=True, null=True)
    language = models.CharField(max_length=20, blank=True, null=True)
    description = models.CharField(max_length=100000, blank=True, null=True)
    duration = models.CharField(max_length=20, blank=True, null=True)
    release_date = models.CharField(max_length=20, blank=True, null=True)
    added_date = models.CharField(max_length=20, blank=True, null=True)
    update_date = models.CharField(max_length=20, blank=True, null=True)
    source = models.CharField(max_length=50, blank=True, null=True)
    archives = models.CharField(max_length=200, blank=True, null=True)
    materials = models.CharField(max_length=200, blank=True, null=True)
    subcategory = models.ManyToManyField(SubCategory, blank=True, related_name='course')

    class Meta:
        ordering = ['-added_date']

    def get_absolute_url(self):
        return reverse('course_detail_url', kwargs={'id_id': self.id})

    def __str__(self):
        return self.title


class Video(models.Model):
    links = models.CharField(max_length=1000, db_index=True, unique=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    def __str__(self):
        return '{}  {}'.format(self.course.title, self.links)
