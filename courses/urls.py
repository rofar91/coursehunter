from django.urls import path
from .views import *

urlpatterns = [
    path('', courses_list, name='courses_list_url'),
    path('course/<str:id_id>/', CourseDetail.as_view(), name='course_detail_url'),
    path('categories/', categories_list, name='categories_list_url'),
    path('category/<str:id_id>/', CategoryDetail.as_view(), name='category_detail_url'),
]
