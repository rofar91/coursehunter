import scrapy


class CourseItem(scrapy.Item):
    title = scrapy.Field()
    image = scrapy.Field()
    language = scrapy.Field()
    description = scrapy.Field()
    duration = scrapy.Field()
    release_date = scrapy.Field()
    added_date = scrapy.Field()
    update_date = scrapy.Field()
    source = scrapy.Field()
    link_to_video = scrapy.Field()
    archive_all_videos = scrapy.Field()
    materials = scrapy.Field()
    category = scrapy.Field()
    subcategory = scrapy.Field()
