import scrapy
from scrapy.loader import ItemLoader
from ..items import CourseItem
import logging

BASE_URL = 'https://coursehunter.net'
EMAIL = 'dima_a_91@mail.ru'
PASSWORD = '13071991'


class CoursehunterSpider(scrapy.Spider):
    name = "coursehunter"
    start_urls = [
        BASE_URL + '/sign-in',
        ]

    logging.basicConfig(filename='loger.log', level=logging.DEBUG)
    logging.debug('This message should go to the log file')
    logging.info('So should this')
    logging.warning('And this, too')

    def parse(self, response):
        yield scrapy.FormRequest.from_response(
            response,
            formxpath='//form[@class="auth"]',
            formdata={
                'e_mail': EMAIL,
                'password': PASSWORD,
            },
            callback=self.after_login
        )

    def after_login(self, response):
        yield scrapy.Request(BASE_URL + '/categories', self.parse_category)

    def parse_category(self, response):
        category = response.xpath('//ul[@class="drop-menu drop-menu-left"]')

        if category.xpath('//li[@class="drop-item drop-menu-has-child"]').getall():  # подкатегории
            hrefs = category.xpath('//li[@class="drop-item drop-menu-has-child"]/a/@href').getall()
            for href in hrefs:
                yield response.follow(href, self.parse_subcategory)

        if category.xpath('//li[@class="drop-item "]').getall():  # если курс не имеет подкатегорий
            hrefs = category.xpath('//li[@class="drop-item "]/a/@href').getall()
            for href in hrefs:
                yield response.follow(href, self.parse_course)

    def parse_subcategory(self, response):
        for subcategory in response.xpath('//div[@class="standard"]//a/@href').getall()[1:]:
            yield scrapy.Request(subcategory, self.parse_course)

    def parse_course(self, response):
        for bloks in response.xpath('//div[@class="course-list"]/article'):
            course = bloks.xpath('.//div[@class="course-details-bottom"]/a/@href').get()
            category_name = response.xpath(
                '//div[@class="breadcrumbs hero-breadcrumbs"]/span[position()>1]//span/text()').get()
            subcategory_name = response.xpath(
                '//div[@class="standard"]//a[@class="chip-item chip-item-active"]/span/text()').get()
            image = bloks.xpath('.//picture[@class="course-figure"]//img/@src').get()
            language = bloks.xpath('.//div[@class="course-lang"]/span/text()').get()
            duration = bloks.xpath('.//div[@class="course-duration"]/span/text()').get()
            yield response.follow(
                course, self.parse_page,
                meta={
                    'subcategory': subcategory_name,
                    'category': category_name,
                    'image': image,
                    'language': language,
                    'duration': duration,
                }
            )

        next_page = response.xpath('//a[@rel="next"]/@href').get()
        if next_page is not None:
            yield response.follow(next_page, self.parse_course, dont_filter=True)

    def parse_page(self, response):
        l = ItemLoader(item=CourseItem(), response=response)
        subcategory = response.meta.get('subcategory')
        category = response.meta.get('category')
        image = response.meta.get('image')
        language = response.meta.get('language')
        duration = response.meta.get('duration')
        l.add_value('subcategory', subcategory)
        l.add_value('category', category)
        l.add_value('image', image)
        l.add_value('language', language)
        l.add_xpath('title', '//h1[contains(@class, "hero-title")]/text()')
        l.add_xpath('description', '//div[@class="course-wrap-description"]')
        l.add_value('duration', duration)
        l.add_xpath('release_date', '//div[text() = "Дата выхода"]/following-sibling::div/text()')
        l.add_xpath('added_date', '//div[text() = "Дата добавления"]/following-sibling::div/text()')
        l.add_xpath('update_date', '//div[text() = "Дата обновления"]/following-sibling::div/text()')
        l.add_xpath('source', '//a[@class="hero-source"]/text()')
        l.add_xpath('link_to_video', '//li[@class="lessons-item"]/link[@itemprop="url"]/@href')
        l.add_xpath('archive_all_videos', '//div[@class="course-wrap-bottom"]/span[@class="ml-15"]/a[position()=1]/@href')
        l.add_xpath('materials', '//div[@class="course-wrap-bottom"]/span[@class="ml-15"]/a[position()=2]/@href')
        yield l.load_item()
