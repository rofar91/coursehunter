import psycopg2
from pytils.translit import slugify


class PostgreSQLPipeline(object):

    def open_spider(self, spider):
        hostname = 'localhost'
        username = 'admin'
        password = '13071991'  # your password
        database = 'courses'
        self.connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
        self.cur = self.connection.cursor()

    def close_spider(self, spider):
        self.cur.close()
        self.connection.close()

    def process_item(self, item, spider):
        # store category
        category_slug = slugify(item['category'][0])
        self.cur.execute("insert into courses_category(category_name, slug) "
                         "values(%s, %s) on conflict do nothing",
                         (item['category'][0], category_slug))
        self.connection.commit()

        # store subcategory
        self.cur.execute("select id from courses_category "
                         "where category_name=%s",
                         (item['category'][0],))
        id_category = self.cur.fetchone()

        subcategory = item['subcategory'][0]
        if subcategory == 'N':  # NULL
            subcategory = item['category'][0]
        subcategory_slug = slugify(subcategory)
        self.cur.execute("insert into courses_subcategory(subcategory_name, category_id, slug) "
                         "values(%s, %s, %s) on conflict do nothing",
                         (subcategory, id_category, subcategory_slug))
        self.connection.commit()

        # store courses
        # Дописать проверку наналичие такихже записей в БД, для того чтобы не дублировались записи
        self.cur.execute("insert into courses_course(title, image, language, description, duration, release_date, "
                         "added_date, update_date, source, archives, materials) "
                         "values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) "
                         "on conflict (title) do update set archives=EXCLUDED.archives, materials=EXCLUDED.materials,"
                         "release_date=EXCLUDED.release_date, added_date=EXCLUDED.added_date,"
                         "update_date=EXCLUDED.update_date",
                         (item['title'][0], item['image'][0], item['language'][0], item['description'][0],
                          item['duration'][0], item['release_date'][0], item['added_date'][0], item['update_date'][0],
                          item['source'][0], item['archive_all_videos'][0], item['materials'][0]))
        self.connection.commit()

        # store link on video
        self.cur.execute("select id from courses_course "
                         "where title=%s",
                         (item['title'][0], ))
        id_course = self.cur.fetchone()

        if item['link_to_video'] != 'NULL':
            for url in item['link_to_video']:
                self.cur.execute("insert into courses_video(links, course_id) "
                                 "values(%s, %s) on conflict do nothing",
                                 (url, id_course))
                self.connection.commit()

        #m2m courses-subcategory
        subcategory = item['subcategory'][0]
        if subcategory == 'N':  # NULL
            subcategory = item['category'][0]
        self.cur.execute("select id from courses_subcategory "
                         "where subcategory_name=%s",
                         (subcategory, ))
        id_subcategory = self.cur.fetchone()

        self.cur.execute("insert into courses_course_subcategory(course_id, subcategory_id) "
                         "values(%s, %s) on conflict do nothing",
                         (id_course, id_subcategory))
        self.connection.commit()

        return item


class DefaultValuesPipeline(object):

    def process_item(self, item, spider):

        for field in item.fields:
            item.setdefault(field, 'NULL')
        return item
